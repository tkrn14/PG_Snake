Namen: Joshua Fett, Philipp Hessler, Thies Kornek

Über uns:
Wir sind erstsemester Studenten im Bereich Medieninformatik. 
Snake ist ein Projekt im Rahmen unseres programmier Kurses. 

Aufgabenverteilung:
Zu beginn möchten wir erwähnen das jeder an allen Teilen des Programmes 
Einfluss genommen hat. Während der Entstehung der einzelnen Versionen
herrschte stetiger Austausch von Ideen und Lösungen. 
Die Aufgabenverteilung ist also ledglich eine grobe Einteilung, welchen 
Schwerpunkt jedes Teammitglied Verwaltet und bearbeitet hat. 

Philipp Hessler(Spielfeld, Food, Schwierigkeitsgrade)
Joshuar Fett(Steuerung)
Thies Kornek(dynamische Schlange, Highscore)