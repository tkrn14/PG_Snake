 Projektname: Snake
 Mitglieder: Thies Kornek, Philipp Hessler, Joshua Fett
 
 Beschreibung:
 Wir haben uns als Ziel gesetzt, das klassische Snake-Spiel mit 
 Visual Studio und Board of Symbols umzusetzen. 
 Dabei halten wir uns an die Grundidee des Spiels, lassen dabei aber eigene Kreativität
 einfließen. 
 
 Grundregeln dafür sind:
 - Das Spielfeld ist beschränkt, wird der Rand berührt ist das Spiel vorbei
 - Berührt die Schlange sich selbst ist das Spiel vorbei
 - Werden "Früchte" eingesammelt wird die Schlange länger
 
 Steuerung: 
 Die Richtung der Schlange kann mit den Tasten w,a,s,d geändert werden.
 Das Programm soll nicht jedes Mal neu gestartet werden, um erneut spielen zu
 können.
 
 -----------------------------------------------------------------------------------------
 
 Herausforderungen:
 Als besonders herausfordernd könnte die Realisierung einer dynamischen Schlange werden.
 Diese soll wie bereits erwähnt stetig wachsen.
 Zusätzlich muss die Steuerung per Tastatur unabhängig vom BoS erfasst werden, da es 
 in BoS schwierig sein wird Tastatur eingaben zu erkennen. 
 
 
 
 